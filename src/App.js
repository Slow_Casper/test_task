import React from 'react';
import Button from './Components/Button/Button';
import Stack from './Components/Stack/Stack';
import styles from './AppStyles.module.scss';

function App() {
  return (
    <div className={styles.container}>
      <div className={styles.btnContainer}>
        <Button value={1} />
        <Button value={2} />
        <Button value={3} />
        <Button value='Clear stack' />
      </div>
      <div>
        <Stack />
      </div>
    </div>
  );
}

export default App;
