import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import { clearStack, setStack } from '../../redux/slices/stackSlice';

import styles from './styles.module.scss';

const Button = ({ value }) => {
    const [ isLoading, setIsLoading ] = useState(false);
    
    const dispatch = useDispatch();

    function clickHandler() {
        if(typeof value === "number"){
            dispatch(setStack({ button: value, clickTime: Date.now() }));
        } else {
            dispatch(clearStack());
        }
    }

    return (
        <button className={styles.button} onClick={clickHandler}>
            {typeof value === 'number' ? value + ' sec' : value }
        </button>
    );
};

Button.propTypes = {
    value: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ]).isRequired,
};

export default Button;