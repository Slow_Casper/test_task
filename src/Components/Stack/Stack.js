import React, { memo, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { updateStack } from '../../redux/slices/stackSlice';

import styles from './styles.module.scss';

const Stack = () => {
  const dispatch = useDispatch();
  const stack = useSelector((state) => state.stack.stack);
  
  const hasUnfinishedLogs = stack.some((log) => !log.end);

  useEffect(() => {
    let isMounted = true;

    const processStack = () => {
      const processingLog = stack.find((log) => !log.end);

      if (processingLog) {
        setTimeout(() => {
          if (isMounted) {
            const processingLogIndex = stack.indexOf(processingLog);
            if (processingLogIndex !== -1) {
              dispatch(updateStack(processingLogIndex));
              processStack();
            }
          }
        }, processingLog?.button * 1000);
      }
    };

    if (hasUnfinishedLogs) {
      processStack();
    }

    return () => {
      isMounted = false;
    };
  }, [dispatch, stack, hasUnfinishedLogs]);

  return (
    <div className={styles.container}>
      {stack.length > 0 && stack.map((el) => {
        if(el.end) {
          return (
            <p className={styles.text} key={el.end}>
              Button #{el.button}: 
              <span className={styles.start}>
                {new Date(el.clickTime).toLocaleTimeString()}
              </span>
              -
              <span className={styles.end}>
                {new Date(el.end).toLocaleTimeString()}
              </span>
              ({new Date(el.end - el.clickTime).getSeconds()} sec)
            </p>
          )
        }
      })}
    </div>
  );
};

export default memo(Stack)