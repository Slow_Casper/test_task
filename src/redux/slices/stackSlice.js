import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    stack: [],
}

const stackSlice = createSlice({
    name: 'stack',
    initialState,
    reducers: {
        setStack(state, action) {
            state.stack.push(action.payload);
        },
        clearStack(state) {
            state.stack = [];
        },
        updateStack: (state, action) => {
            const id = action.payload;
            state.stack[id] = {
                ...state.stack[id],
                end: Date.now()
            };
          },
    },
});

export const { setStack, clearStack, updateStack } = stackSlice.actions;
export default stackSlice.reducer;
