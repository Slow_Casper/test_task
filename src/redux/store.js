import { combineReducers, configureStore } from "@reduxjs/toolkit";
import stackSlice from "./slices/stackSlice";


const reducer = combineReducers({
    stack: stackSlice,
});

const store = configureStore({
    reducer,
});

export default store;